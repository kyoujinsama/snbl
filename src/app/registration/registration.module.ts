import { NgModule }    from "@angular/core";
import { FormsModule } from "@angular/forms";

import { RegistrationRoutes } from "./registration.routes";
import { RegistrationService } from "./registration.service";


//The Registration FEATURE (=module)
@NgModule({
    imports:[    
        FormsModule,
        RegistrationRoutes
    ],
    providers:[RegistrationService]
})
export class RegistrationModule{
}