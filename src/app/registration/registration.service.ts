import { Injectable }                     from "@angular/core";
import { Http, Response, RequestOptions } from "@angular/http";
import { Observable }                     from "rxjs/Observable";

import { User } from "./models/user.model";

@Injectable()
export class RegistrationService {
    //initialize the user object used by the form
    private user : User = new User();
    
    private blancoApi:string='https://private-dda55-blancodeveloperassignment.apiary-mock.com/dev/registrations';

    constructor(private http:Http){ }

    getUser():User {
        return this.user;
    }

    // create a new empty user 
    newUser():void {
        this.user = new User();
    }

    //send the user to the backend
    register(body:any):Observable<any>{
        return this.http.post(this.blancoApi,JSON.stringify(body));
    }
}