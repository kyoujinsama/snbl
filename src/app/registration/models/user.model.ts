import { Address } from "./address.model";

export class User {
    sex:string;
    initials:string;
    firstName:string;
    lastName:string;
    dateOfBirth:string;
    countryOfBirth:string;
    socialSecurityNumber:string;
    address:Address;

    constructor(){
        //avoid null references
        this.address = new Address();
    }
}
