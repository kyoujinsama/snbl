import { NgModule }              from "@angular/core";
import { RouterModule, Routes }  from "@angular/router";
import { AboutComponent }        from "app/about/about.component";
import { PageNotFoundComponent } from "app/errors/404.component";
import { HomeComponent }         from "app/home/home.component";

const routes:Routes=[
    { path : '', component : HomeComponent },
    { path : 'about', component : AboutComponent },

    //lazy loading the registration routes
    { path : 'register', loadChildren:'./registration/registration.routes#RegistrationRoutes'},
    
    //fallback route, redirect to 404
    { path : '**', component:PageNotFoundComponent, pathMatch:'full'}
]

@NgModule({
    imports:[
        RouterModule.forRoot(routes)
    ],
    exports:[
        RouterModule
    ]
})
export class AppRoutingModule{

}