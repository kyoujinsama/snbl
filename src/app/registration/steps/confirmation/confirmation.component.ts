import { Component } from "@angular/core";
import { Router }    from "@angular/router";

import { User }                from "../../models/user.model";
import { RegistrationService } from "../../registration.service";
import { Payload }             from "../../models/payload.model";

@Component({
    templateUrl:'confirmation.component.html'
})
export class ConfirmationComponent{
    public user : User;
    
    constructor(private registrationService:RegistrationService,
                private router:Router){
        this.user = registrationService.getUser();
    }

    confirm():void{
        this.registrationService.register(new Payload(this.user))
        .subscribe((res:Response)=>{
            alert(`welcome abord ${this.user.firstName}!`);
            console.log(res);
            this.router.navigate(['/']);
        });
    }
}