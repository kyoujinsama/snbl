import { Component } from "@angular/core";

import { Address }             from "../../models/address.model";
import { RegistrationService } from "../../registration.service";

@Component({
    templateUrl:'contact-details.component.html'
})
export class ContactDetailsComponent {
    public address : Address;

    constructor(private registrationService:RegistrationService){
        this.address = registrationService.getUser().address;
    }
}