# Folder structure#

This is the chosen folder structure for the source code : 

- **App**
  - **About** : the about component.
  - **Errors** : error pages components.
  - **Home**: The home page component.
  - **Navigation**: The navigation component.
  - **Registration** : the registration ***feature*** (a standalone Angular Module).
    - **Models** : models used by the different registration steps components.
    - **Steps**
      - **Confirmation** : The confirmation step (3rd step).
      - **Contact-details**: the contact details step (2nd step).
      - **Personal-details**: the personal details step (1st step).


# Possible Application Improvements#

## Route validation##

Add routing validation for the registration steps in order to prevent direct access to a certain step without going through previous steps successfully first. In order to achieve this we could build a chain-of-responsibility [[1\]](#_ftn1)  structure, *although it is an overkill for something as simple as this use case*. We can then represent steps by @Injectable() state keeping services :

``````typescript
@Injectable()
export class PersonalDetailsStep implements Step {
  constructor(private next : ContactDetailsStep) {
    next.previous = this;
  }
}
``````

which would be managed by a registration wizard keeping track of successful previous steps :

``````typescript
@Injectable()
export class RegistrationWizard {  
  constructor(private firstStep : PersonalDetailsStep){}
  //checks whether we can visi the step passed to the function
  canVisit(step:Step):boolean{}
  //sets the latest validated step
  checkpoint(step:Step):void{}
  //returns the step following the checkpoint (the 3rd step if we had successfully validated the 2nd step for example).
  next():Step{} //return checkpoint.next()
}
``````

We can now listen to the router change event in our registration components to check whether they should be accessed or not :

``````typescript
//...
if(!this.wizard.canVisit(this.step)){
  route.navigate([this.wizard.next().url])
}
``````



------

[[1\]](#_ftnref1) [https://en.wikipedia.org/wiki/Chain-of-responsibility_pattern](https://en.wikipedia.org/wiki/Chain-of-responsibility_pattern)

## Use Modals for confirmation

Confirm leaving page when a form is not submitted using a modal.



# Using the source code#

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.2.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
