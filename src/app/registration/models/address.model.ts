
export class Address{
    line1:string;
    line2:string;
    postalCode:string;
    city:string;
    country:string;
    phone:string;
    email:string;
}