import { NgModule }             from "@angular/core";
import { CommonModule }         from "@angular/common";
import { FormsModule }          from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { HttpModule }           from "@angular/http";

import {PersonalDetailsComponent} from './steps/personal-details/personal-details.component';
import {ContactDetailsComponent}  from './steps/contact-details/contact-details.component';
import {ConfirmationComponent}    from './steps/confirmation/confirmation.component';

const routes : Routes = [
    {
        path:'register', 
        children:[
            //redirect by default to the personal-details route
            { path: '', redirectTo: 'personal-details', pathMatch: 'full' },
            { path: 'personal-details', component: PersonalDetailsComponent },
            { path: 'contact-details', component: ContactDetailsComponent },
            { path: 'confirmation', component: ConfirmationComponent }
        ]
    }
]

@NgModule({
    imports:[
        FormsModule,
        CommonModule,
        HttpModule,
        RouterModule.forChild(routes)
    ],
    exports:[
        RouterModule
    ],
    declarations:[
        PersonalDetailsComponent,
        ContactDetailsComponent,
        ConfirmationComponent
    ]
})
export class RegistrationRoutes {
}