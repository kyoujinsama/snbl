import { Component }  from "@angular/core";
import { Observable } from "rxjs/Observable";

import { RegistrationService } from "../../registration.service";
import { User }                from "../../models/user.model";

@Component({
    templateUrl:'personal-details.component.html'
})
export class PersonalDetailsComponent{
    public user:User;
    constructor(private registrationService:RegistrationService){
        this.user = registrationService.getUser();
    }

}