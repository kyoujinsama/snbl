import { SnajahiPage } from './app.po';

describe('snajahi App', () => {
  let page: SnajahiPage;

  beforeEach(() => {
    page = new SnajahiPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
