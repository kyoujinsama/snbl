import { User } from "./user.model";

export class Payload {
    constructor (user : User){
        this.parties.push(new Party(user));
    }
    type: string='INDIVIDUAL';
    parties: Party[]=[];
    documents: any[];
}
class Party {
    constructor(user:User){
        this.naturalPerson=user;
    }
    naturalPerson:User;
    legalEntity:any=null;
    parties:any[];
    documents:any[];
}