import { NgModule }             from "@angular/core";
import { BrowserModule }        from "@angular/platform-browser";
import { FormsModule }          from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { AppComponent }           from "app/app.component";
import { AboutComponent }         from "app/about/about.component";
import { NavigationComponent }    from "app/navigation/navigation.component";
import { HomeComponent }          from "app/home/home.component";
import { PageNotFoundComponent }  from "app/errors/404.component";
import { AppRoutingModule }       from "app/app.routes";
import { RegistrationModule }     from "app/registration/registration.module";

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    HomeComponent,
    AboutComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RegistrationModule,
    AppRoutingModule //Should be the last routing module imported
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
